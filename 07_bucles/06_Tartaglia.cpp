#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define N 20

unsigned dv(unsigned celda[N][N], int f, int c) {
    if (f<0 || c<0 || c>f)
        return (unsigned) 0;
    return celda[f][c];
}

/* Función punto de entrada */
int  main(){

    int num;

    unsigned tart[N][N];
    bzero(tart, sizeof(tart));/*Pone a 0 la memoria*/

    printf ("Introduce un numero: ");
    scanf  (" %i",&num);

    tart[0][0] = 1;
    /*Bucles anidados*/
    for (int fila=1; fila<num; fila++)
        for (int col=0; col<=fila; col++)
            tart[fila][col] = dv(tart, fila-1, col-1) + dv(tart, fila-1, col);

    for (int fila=0; fila<num; fila++){
        for (int col=0; col<=fila; col++)
             printf (" %i", tart[fila][col]);
        printf ("\n");
    }

    printf ("\n");
    return EXIT_SUCCESS;
}
