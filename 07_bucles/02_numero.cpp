#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define GRADOS

#ifdef GRADOS
#define K 1
const char *unidad = "º";
#else
#define K M_PI / 180
const char *unidad = "rad";
#endif

int  main (){

   for (double grados=0; grados<K*360.; grados+=K*.5){
        printf ("%.2lf %s\n", grados,unidad);/*el (double) es un ejemplo de moldeR*/
    }

    return EXIT_SUCCESS;
}

