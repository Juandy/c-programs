#include <stdio.h>
#include <stdlib.h>
#include <math.h>



#define cuadrado 1
#define triangulo 2

int  main (){

    int col,fil,num;
    int opcion;

    printf("Cuadrado (1) o Triangulo(2): ");
    scanf("%i",&opcion);
    system ("clear");

    switch (opcion){
        case cuadrado:
            printf ("Ingrese un numero: ");
            scanf  (" %i",&num);
            printf  ("\n");
            for(col=0; col<num ; col++){
                for(fil=0; fil<num; fil++)
                    printf(" *");
                printf("\n");
            }
            break;
        case triangulo:
            printf ("Ingrese un numero: ");
            scanf  (" %i",&num);
            printf  ("\n");
            for(fil=0; fil<num ; fil++){
                for(col=0; col<=fil; col++)
                    printf(" *");
                printf("\n");
            }
            break;
        default:
            printf ("Try again \n");
            break;

    }

    return EXIT_SUCCESS;
}
