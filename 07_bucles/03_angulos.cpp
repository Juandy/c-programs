#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define GRADOS
#ifdef GRADOS
#define K 1
const char *unidad = "º";
#else
#define K M_PI / 180
const char *unidad = " rad";
#endif

double arad (double angulo){
#ifdef GRADOS
    return M_PI * angulo / 180;
#else
    return angulo;
#endif
}

/* Función punto de entrada */
int  main(){

    for (double angulo=0; angulo<K*360.; angulo+=K*.5)
        printf ("%.2lf%s:\tcos(%.2lf) = %.6lf\n",
                angulo, unidad, angulo,
                cos( arad(angulo) ));

    return EXIT_SUCCESS;
}
