#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int /*se llama*/ main (){

    int l = 5;

    for (int f=0;f<l;f++){
        for (int c=0;c<l; c++)
            printf (" *");
        printf (" \n");
    }
    printf (" \n");

    for (int f=0;f<l;f++){
        for (int c=0;c<=f; c++)
            printf (" *");
        printf (" \n");
    }

    printf ("\n");

    for (int f=0;f<l;f++){
        for (int c=0;c<l; c++)
            if (c==0 || c==l-1 || f==0 || f==l-1)
            printf ("*");
            else
                printf (" ");
        printf (" \n");
    }


    return EXIT_SUCCESS;
}
