#include <stdio.h>
#include <stdlib.h>

int main (){

    unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23,};//numeros enteros sin signo se representaria con un %u (sig linea)
    unsigned elementos  = (unsigned) sizeof(primo) / sizeof(int);
    unsigned  *peeping = primo;
    //lo mismo que contenga primo se guardara en peeping
    char *tom = (char *) primo;
    unsigned  **police = &peeping;//**police contienen la direccion de la direccion de un unsigned

    printf( "PRIMO:\n"
            "======\n"
            "Localización (%p)\n"
            "Elemetos: %u [%u..%u]\n"
            "Tamaño: %lu bytes.\n\n",
            primo,
            elementos,
            primo[0], primo[elementos-1],
            sizeof (primo));

    printf( " 0:%u\n 1:%u\n\n", peeping [0], peeping[1]);//anotación de matrices
    printf( " 0:%u\n 1:%u\n\n", *peeping, *(peeping+1) ); //anotación de puntero
    printf( "Tamaño: %lu bytes.\n\n", sizeof(peeping) );
    // (*) alli donde apunta en este caso peeping (&) direccion de...
    // lu significa que el numero entero no devulve 4 bytes si no 8 bytes un entero ocupa el doble de lo normal

    /*Memory Dump - Volcado de Memoria */
    for  (int i=0; i<sizeof(primo);i++)
        printf ("%02X", *(tom+i));// %02X le decimos que nos imprima con dos difras Hexadecimales y nos rellene los huecos con 0
    printf (" \n\n");

    printf ( "Police contine  %p\n",  police);//lo que contiene police
    printf ( "Peeping  contine  %p\n", *police);//alli donde apunta(en peeping)
    printf ( "Primo [0] contine  %u\n", **police);//alli donde apunta police es, alli donde apunta police

    printf (" \n\n");



    return EXIT_SUCCESS;
}
