#include <stdio.h>
#include <stdlib.h>

#define N 15

/* Función punto de entrada */
int  main(){
    int div[N];
    int num = 15, n_div=0;

    /* Encontrar los divisores */
    for (int pd=num/2; pd>1; pd--)
        if (num % pd == 0)
            div[n_div++] = pd;

    /* Imprimir el resultado */
    for (int i=0; i<n_div; i++)
        printf("%i ", div[i]);

    printf (" \n");

    return EXIT_SUCCESS;
}
