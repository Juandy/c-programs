#include <stdlib.h>
#include <stdio.h>

int main(){

	int numerador=78, 
	    denominador=36;

	for (int pd=numerador/2; pd>1; pd--)
		if (numerador % pd == 0 && denominador % pd ==0){
			numerador /= pd; /*Es numerador=numerador/pd  es poner el nuevo numerador en el anterior 78/pd(2) = nuevo  numerador (39)*/
			denominador /= pd;
		}
	printf ("%i / %i \n", numerador, denominador);

	return EXIT_SUCCESS;
}
