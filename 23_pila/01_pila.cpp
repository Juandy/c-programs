#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

#define MAX 0x10

void push (int numero, int p[MAX], int *cima) {
    if (*cima < MAX) {
            p[*cima] = numero;
            (*cima)++;
        } else
        fprintf (stderr, "Pila llena intentando escribir un %i.\n", numero);
}

int pop (int p[MAX], int *c) {
    int resultado = p[*c-1];
    if (*c <= 0) {
            fprintf (stderr, "La pila está vacía.\n");
            return -666;
        }
    (*c)--;

    return resultado;
}

void titulo () {
    system ("clear");
    system ("toilet -f pagga PILA");
    printf ("\n");
}

void imprimir (int pila[MAX], int cima){
    printf ("PILA\n");
    printf ("====\n");
    for (int i=0; i<cima; i++)
        printf ("\t%i\n", pila[i]);
    printf ("\n");
}

int  main(int argc, char *argv[]){
    int pila[MAX];
    int cima = 0;
    int nuevo;
    bool fin;
    int d = 0;
    int devuelto = 0;

    do {
         titulo ();
         imprimir(pila, cima);
         printf ("Devuelto: %i\n", devuelto);
         printf ("Entrada: ");
         d = scanf (" %i", &nuevo);
         __fpurge(stdin);
         if (d)
             push (nuevo, pila, &cima);
         else
             devuelto = pop(pila, &cima);
        } while (!fin);

    return EXIT_SUCCESS;
}

