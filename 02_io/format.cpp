#include <stdio.h>
#include <stdlib.h>

int /*se llama*/ main (){

    printf("%c",97);/*codigo Acii del caracter*/
    printf("%c",0x61);	    
    printf("%c",'a');/*constante de mismo caracter*/	    

    puts ("NULL");
    printf("%i",97);/*IMpime un 97 como tal*/
    /*
    4 =>'4' 
    4 + '0'
    4 + 0x30
    */
    /*
    scanf("%i" )

    '4'
    '4'-'0 '=> 4
    '7'
    4*10 => 40
    '7' - '0' => 7
    40 + 7 = 47
    '5'
    47 * 10 => 470
    '5' - '0' => 5
    470 + 5
    475
*/

    puts ("");
    printf ("%i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n",1,2,3,4,5,6,7,8,9,10,11,12,13,14);


    printf("%lf\n",3.2);
    printf("%.2lf\n",3.2);
    printf("%6.2lf\n",3.2);

    printf ("\thola\n");/*\t tabula, pero lo lee como 4 espacios*/
    int numero;/* Siempre se declara alprincipio */
    printf("Num: ");
    scanf(" %i",&numero);
    printf(" Numero => [%p]: %i\n",&numero, numero);/*%p sirve como puntero para apuntar a la memoria y esteb te imprime donde esta guardada la variable numero y se pone &numero porque le estamos indicando el ugar de memoria donde guardamos la variable anterior*/
    printf(" Linea %i\n",47);/*Lo que valga numero*/
   
    int n1, n2;
    printf ("Num [nonumero] Numero: ");
    scanf (" %i %*i %i",&n1, &n2);/*el %*i es un caracter de supresión de asignación*/

    /*
    ejemplo
    int dia, annio;
    printf ("Nacimiento dd/mm/aaaa: ");
    scanf (" %i/ %*i/ %i",&dia, &annio);
    */

    char hex[32];
    scanf("%[0-9a-fA-F]",hex);/*saca todo los nuemero del 0 al 9 y  de la 'a' a la 'f'*/
    
    scanf("%[^0-9a-fA-F]",hex);/*conjjunto de seleccion inverso*/

    return EXIT_SUCCESS;
}
