
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 13
#define BTU 100000 /*Basic Time Unit*/ 

int main(){
    int duracion[VECES] = {0, 2, 8, 2, 1, 8, 2, 1, 2, 1, 2, 2};
     
    //scanf("%i", &duracion);
    /* bucle for: debe valer para repetir cosas*/
    for (int i=0; i<VECES; i++) {
	usleep(duracion[i] * BTU);
	fputc('\a', stderr);
    }

    return EXIT_SUCCESS;
}
