#include <stdio.h>
#include <stdlib.h>

int main(){
	int n1, multiplica;

	printf("Dime un numero para multiplicar: ");
	scanf("%i", &n1);
	
	printf("%i * 1 = %i \n", n1, n1 * 1);
	printf("%i * 2 = %i \n", n1, n1 * 2);
	printf("%i * 3 = %i \n", n1, n1 * 3);
	printf("%i * 4 = %i \n", n1, n1 * 4);
	printf("%i * 5 = %i \n", n1, n1 * 5);
	printf("%i * 6 = %i \n", n1, n1 * 6);
	printf("%i * 7 = %i \n", n1, n1 * 7);
	printf("%i * 8 = %i \n", n1, n1 * 8);
	printf("%i * 9 = %i \n", n1, n1 * 9);
	printf("%i * 10 = %i \n", n1, n1 * 10);
	
	return EXIT_SUCCESS;
}
