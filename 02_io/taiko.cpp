#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int  main (){
    /*printf tiene escritura bufferizada*/ 
    
    //puts("hola");
    //printf("hola\n");
    fprintf(stderr, "\a");
    //sleep(1);
    usleep(100000);
    fputc('\a', stderr);
    usleep(100000);
    //sleep(1);
    printf("\a\n");

    return EXIT_SUCCESS;
}
