#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int /*se llama*/ main (){
    //bool los valores solo pueden ser true o false

    bool r = false, y = false, b = false;
    char respuesta;

    printf("Ves rojo? (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        r = true;

    printf("Ves amarillo? (s/n): ");
    scanf(" %c", &respuesta);
    y = respuesta == 's';

    printf("Ves azul? (s/n): ");
    scanf(" %c", &respuesta);
    b = respuesta == 's';



    if(r)
        if(y)
            if(b)
                printf ("El color el blanco\n");/*veo los tres colores, veo blanco*/
            else
                printf ("El color es naranja\n");/*si no veo el azul(b) solo veria el rojo(r) y amarillo(y)que es naranja*/
        else
            if(b)
                printf ("El color es morado\n");/*si no veo (y) estoy viendo (r) y (b), es decir morado*/
            else
                printf("El color es rojo\n");/*si no veo ni (y) ni (b) solo (r), seria rojo*/

    else
        if (y)
            if(b)
                printf("El color es verde\n");/*Si veo (y) y (b) pero no (r), veo verde*/
            else
                printf("El color es amarillo\n");/*si veo (y) pero no (b) ni (r), solo veria amarillo*/
        else
            if(b)
                printf("El color es Azul\n");/*si no veo (r) ni (y) solo veo (b), veria azul*/
            else
                printf("El color es negro\n");/*si no veo ni (r) ni (y) ni (b), veria negro*/

    //El else me anula el color anteior si es
    //if(r)
    //  if(y)
    //    if(b)
    //    else (este else me anula b y por eso veo  en naranja)


    return EXIT_SUCCESS;
}
