#ifndef __ESTRELLA_H__
#define __ESTRELLA_H__

#define N 0x100

struct TEstrella {
	double ascension, declinacion;
	double distancia, diametro, masa;
	double edad;
	double brillo;
};

struct TPila {
	struct TEstrella *datos[N];
	int cima;
};
#endif

