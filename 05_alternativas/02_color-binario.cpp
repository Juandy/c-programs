#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

// Función punto de entrada
int /*se llama*/ main (){

    char respuesta;
    int color=0;

    printf("¿Ves el color Rojo?: (S/N); \n");
        scanf (" %c",&respuesta);
        if (respuesta == 's')
            color |= ROJO;

    printf("¿Ves el color Amarillo?: (S/N); \n");
        scanf (" %c",&respuesta);
        if (respuesta == 's')
            color |= AMAR;

    printf("¿Ves el color Azul?: (S/N); \n");
        scanf (" %c",&respuesta);
        if (respuesta == 's')
            color |= AZUL;

  switch(color) {
    case 0:
        printf("El color es Negro.\n");
        break;
     case 1:
        printf("El color es Azul.\n");
        break;
     case 2:
        printf("El color es Amarillo.\n");
        break;
     case 3:
        printf("El color es Verde.\n");
        break;
     case 4:
        printf("El color es Rojo.\n");
        break;
     case 5:
        printf("El color es Morado.\n");
        break;
     case 6:
        printf("El color es Naranja.\n");
        break;
     case 7:
        printf("El color es Blanco.\n");
        break;
  }


    return EXIT_SUCCESS;
}
