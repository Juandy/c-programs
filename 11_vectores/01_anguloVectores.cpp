#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// Función punto de entrada
int  main (){

    int ax,
        bx,
        ay,
        by;
    double resultado1,
           resultado2,
           angulo;

    printf ("Cuanto vale  Ax y Ay: ");
    scanf  ("%i,%i",&ax,&ay);

    printf ("Cuanto vale Bx y By: ");
    scanf  ("%i,%i",&bx,&by);

    resultado1= (ax * bx) + (ay * by);
    resultado2=(sqrt(pow(ax,2)+pow(ay,2))*(sqrt(pow(bx,2)+pow(by,2))));
    angulo = acos(resultado1/resultado2);
    angulo *=180/M_PI;

    printf ("El angulo es :%.2lfº \n", angulo);


    return EXIT_SUCCESS;
}
