#include <stdio.h>
#include <stdlib.h>

#define D 3
#define E 2

//Función punto de entrada
int main(){

    double a[D][D],
    b[D][D],
    c00,
    c10;

    //ENTRADA DE DATOS
    for(int i=0; i<E; i++){
        printf("Dime los numeros de la primera matriz:\n");
        scanf("%lf %lf %lf", &a[i][0], &a[i][1], &a[i][2]);
    }

    for(int i=0; i<D; i++){
        printf("Dime los numeros de la segunda matriz:\n");
        scanf("%lf", &b[i][0]);
    }

    //CALCULO DE DATOS
    for(int i=0;i<D;i++ ){
        c00 += a[0][i] * b[i][0];
        printf (" %.0lf ", c00);
    }
    printf ("\n");
    for(int i=0;i<D;i++ ){
        c10 += a[1][i] * b[i][0];
        printf (" %.0lf ", c10);
    }
    printf ("\n");




    //DATOS DEL ARRAY
    printf("Datos del array a\n");
    for(int i=0;i<E;i++)
        printf("  %.0lf, %.0lf, %.0lf\n", a[i][0], a[i][1], a[i][2]);

    printf("Datos del array b\n");
    for(int i=0;i<D;i++)
        printf(" %.0lf\n", b[i][0]);

    return EXIT_SUCCESS;
}
