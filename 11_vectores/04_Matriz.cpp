#include <stdio.h>
#include <stdlib.h>

// Función punto de entrada
int  main (){

    double x,y,z,
           x1,y1,z1,
           x2,y2,z2,
           resultado;

    double A[3][3]{
    };

    printf ("Dime la 1 fila: ");
    scanf  ("%lf %lf %lf",&x,&y,&z);
    printf ("Dime la 2 fila: ");
    scanf  ("%lf %lf %lf",&x1,&y1,&z1);
    printf ("Dime las 3 fila: ");
    scanf  ("%lf %lf %lf",&x2,&y2,&z2);

    resultado = x*y1*z2 + x1*y2*z + x2*z1*y;


    printf ("%.0lf %.0lf %.0lf \n"
            "%.0lf %.0lf %.0lf \n"
            "%.0lf %.0lf %.0lf \n", x,x1,x2,y,y1,y2,z,z1,z2);

    printf ("%.0lf\n",resultado);

    return EXIT_SUCCESS;
}
