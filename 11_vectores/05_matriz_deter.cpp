#include <stdio.h>
#include <stdlib.h>

#define D 3

//Función punto de entrada
int main(){

    double a[D][D],
           multiplicacion,
           multiplicacion1,
           determinante,
           determinante1,
           determinanteF;

    //ENTRADA DE DATOS
    for(int i=0;i<D;i++){
        printf(" Dime el vector %i:\n", i + 1);
        scanf(" %lf %lf %lf", &a[i][0], &a[i][1], &a[i][2]);
    }


    //CALCULO DE DATOS
    for(int f=0; f<D; f++){
      multiplicacion = 1;
      for (int d=0; d<D; d++)
          multiplicacion *= a[(f+d)%D][0+d];
      determinante += multiplicacion;
    }
     for(int f=0; f<D; f++){
      multiplicacion1 = 1;
      for (int c=0; c<D; c++)
          multiplicacion1 *= a[(f+c)%D][2-c];
      determinante1 += multiplicacion1;
    }
     determinanteF = determinante - determinante1;

    //SALIDA DE DATOS
    printf(" El resultado es: %.0lf \n", determinante);
    printf(" El resultado es: %.0lf \n", determinante1);
    printf(" El determinante final es : %.0lf",determinanteF);


    //DATOS DEL ARRAY
    printf(" Datos del array\n");
    for(int i=0;i<3;i++)
        printf(" %.0lf %.0lf %.0lf\n", a[i][0], a[i][1], a[i][2]);


    return EXIT_SUCCESS;

}
