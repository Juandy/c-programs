#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Función punto de entrada
int main(){

    double x,
           y,
           ang,
           r00,r01,r10,r11,
           resultado1,
           resultado2;

    printf ("Dime las nuevas cordenadas X, Y: \n");
    scanf  ("%lf %lf",&x,&y);
    printf ("Dime el angulo de rotacion: \n");
    scanf  ("%lf",&ang);

    r00= cos(ang*M_PI/180);
    r01= sin(ang*M_PI/180);
    r10= sin(ang*M_PI/180);
    r11= cos(ang*M_PI/180);

    printf ("%.2lf %.2lf %.2lf %.2lf\n",r00,r01,r10,r11);

    resultado1 = (x*r00)-(y*r10);
    resultado2 = (x*r01)+(y*r11);

    printf("El nuevo angulo es %.2lf %.2lf \n",resultado1,resultado2);










    return EXIT_SUCCESS;
}
