#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX 0x100
#define N 10

int  main (int argc, char *argv[]){

    char nombre [N][MAX];
    for(int i=0;i<N;i++){
        printf ("Introduce la %i palabra: \n", i);
        scanf  ("%s\n", nombre[i]);
    }

    FILE *pf;

    if ( !(pf = fopen ("nombres.txt", "w")) )
        fprintf (stderr, "Couldn't find your %s\n", nombre[MAX]);
    for(int i=0;i<N;i++)
        fprintf(pf, " %s", nombre[i]);

    fclose (pf);
    return EXIT_SUCCESS;
}
