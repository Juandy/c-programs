#include <stdio.h>
#include <stdlib.h>

#define N 9
#define FILENAME "fibonacci.txt"

int  main (int argc, char *argv[]){

    int fibonacci[N];
    FILE *pf;

    if(!(pf= fopen(FILENAME,"wb")) )// La b significa binario, wb es write binary, lo escribira todo en binario
        fprintf (stderr, "Couldn't find your %i\n", fibonacci[sizeof fibonacci]);
    fread (fibonacci, sizeof(int),sizeof(fibonacci)/sizeof(int),pf);
    fclose(pf);


    for(int i=0; i<N; i++)
        printf ("%i", fibonacci[i]);

    printf("\n");


    return EXIT_SUCCESS;
}


// para leer el documento correctamente --> od -tx fibonacci.txt ||od -tx1 fibonacci.txt

