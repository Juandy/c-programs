#include <stdio.h>
#include <stdlib.h>

#define N 9
#define FILENAME "fibonacci.txt"

int rellenar (int fi[N], int i){
    if(i == 1) {
      fi[0] = 1;
      return fi[1] = 1;
    }
    return fi[i] = rellenar(fi,[i-1]) + fi[i-2];

}

int  main (int argc, char *argv[]){

    int fibonacci[N];
    FILE *pf;

    rellenar (fibonacci,N-1);

    if(!(pf= fopen(FILENAME,"wb")) )// La b significa binario, wb es write binary, lo escribira todo en binario
        fprintf (stderr, "Couldn't find your %i\n", fibonacci[sizeof fibonacci]);
    fwrite (fibonacci, sizeof(int),sizeof(fibonacci)/sizeof(int),pf);
    fclose(pf);


    return EXIT_SUCCESS;
}


// para leer el documento correctamente --> od -tx fibonacci.txt ||od -tx1 fibonacci.txt

