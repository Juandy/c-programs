#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX 0x100
#define N 10

int  main (int argc, char *argv[]){

    char nombre [N][MAX];

    FILE *pf;

    if ( !(pf = fopen ("nombres.txt", "r")) )
        fprintf (stderr, "Couldn't find your %s\n", nombre[MAX]);
    for(int i=0;i<N;i++)
        fscanf(pf, " %s", nombre[i]);/*Solo lee palabras con el fgets leemos lineas*/

    fclose (pf);

    for(int i=0;i<N;i++)
        printf(" %s", nombre[i]);

    printf("\n");

    return EXIT_SUCCESS;
}
