#include <stdio.h>
#include <stdlib.h>

#define N 10
#define OUT "frase.txt"
#define MAX_LIN 0x1000

int  main (int argc, char *argv[]){

    FILE *pf;
    char buffer[MAX_LIN];

    if(!(pf=fopen(OUT,"w")) ){                                  //si no hay pf o es igual a 0 se cumple lo siguiente si pf es 1 u distinto a 0 no mostrara el mensaje 
        fprintf (stderr, "No he podido abrir %s.\n", OUT);
        return EXIT_FAILURE;

    }
    printf ("Dime, las %i frases más importantes de tu vida\n",N);

    for(int i=0; i<N; i++){
        printf ("Frase: ");
        fgets (buffer, MAX_LIN, stdin);
        fprintf (pf, buffer);
    }

    fclose (pf);
    return EXIT_SUCCESS;
}

