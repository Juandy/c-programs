#include <stdio.h>
#include <stdlib.h>

#define FILENAME "fibonacci.txt"

int  main (int argc, char *argv[]){

    int fibonacci[]= {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
    FILE *pf;

    if(!(pf= fopen(FILENAME,"wb")) )// La b significa binario, wb es write binary, lo escribira todo en binario
        fprintf (stderr, "Couldn't find your %i\n", fibonacci[sizeof fibonacci]);
    fwrite (fibonacci, sizeof(int),sizeof(fibonacci)/sizeof(int),pf);
    fclose(pf);


    return EXIT_SUCCESS;
}


// para leer el documento correctamente --> od -tx fibonacci.txt ||od -tx1 fibonacci.txt

