
#include <stdio.h>
#include <stdlib.h>



int  main (int argc, char *argv[]){


    const char * cancion;
    char c;
    FILE *pf;

    if(argc<2)
        return EXIT_FAILURE;

    cancion= argv[1];
    if (!(pf= fopen(cancion, "r")) )
        return EXIT_FAILURE;

    while ( (c = fgetc(pf)) != EOF)  //lee el siguiente caracter del tubo y devulve un caracter sin signo convertido en entero o EOF end of file si hay un error de lectura o ha llegado al final del fichero
        printf ("%c",c);

    fclose (pf);

    return EXIT_SUCCESS;
}
