#include <stdio.h>
#include <stdlib.h>

#define NOMBRE "cancion.txt"

//la direccion en memoria de song no puede variar
const char * song= "\n\
                    Don't think sorry is easily said\n\
                    Don't try turning tables instead\n\
                    You've taken lots of chances before\n\
                    But ain't gonna give anymore\n\
                    Don't ask me, that's how it goes\n\
                    'Cause part of me knows what you're thinkin'\n\
                    ";
void print_usage (){
    printf ("Esto se usa asi\n");
}
void informo (const char *mssg){
    print_usage ();
    fprintf(stderr, "%s\n", mssg);
    exit(1);
}
int main (int argc, char *argv[]){



    FILE*fichero;                                    //variable capaz de apuntar a un tubo


    if( !(fichero = fopen (NOMBRE, "w")))            //crear un tubo, se pone el nombre del fichero y si sera de Lectura"r" o Escritura"w"
        informo ("No se ha podido abrir el fichero");
    fprintf (fichero, "%s", song);                   //fprintf especificamos el tubo de salida
    fclose (fichero);                                //Cerramos el tubo anteriormente abierto

    return EXIT_SUCCESS;
}
