#include "queue.h"

int error = vacio;

void push (struct TCola *cola, int dato) {
    error = 0;

    if (cola->cima - cola->base >= MAX) {
        error = lleno;
        return;
    }

    cola->data[cola->cima++ % MAX] = dato;
}

int shift (struct TCola *cola) {
    error = 0;

    if (cola->cima - cola->base <= 0) {
        error = vacio;
        return error;
    }

    return cola->data[cola->base++];
}

const char *vacia (int error) {
    return error == vacio ? CYAN : RESET;
}

const char *llena (int error) {
    return error == lleno ? RED : RESET;
}


