#include <stdio.h>
#include <stdlib.h>

#define N 10

/*Funcion de entrada*/

int main (){
    /*Entrada de datos*/
    double nota[N];
    double media;

    /*calculos*/
    for (int i=0; i<N;i++){
        printf("Que nota a sacado el alumno %i \n",i);
        scanf ("%lf",&nota[i]);
    }

    for (int i=0;  i<N; i++)
        media += nota[i];
    media /= N;
    /*Salida de datos */

    printf ("Media: %.2lf\n", media);


    return EXIT_SUCCESS;
}
