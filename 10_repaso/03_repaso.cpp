#include <stdio.h>
#include <stdlib.h>

#define N 1000

/*Funcion de entrada*/

int main (){
    /*Entrada de datos*/
    int n_alumnos=0;
    double nota[N],
           entrada,
           media = 0;


    /*calculos*/

    do{
    printf("Nota: ");
    scanf (" %lf",&entrada);
    if(entrada>=0)
        nota[n_alumnos++] = entrada;
    }while (entrada >=0);

    for (int i=0; i<n_alumnos; i++)
        media += nota[i];
    media /= n_alumnos;


    /*Salida de datos */

    printf ("Media: %.2lf\n", media);


    return EXIT_SUCCESS;
}
