#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "buscar.txt"

//Función punto de entrada
int main(){

    long inicio, fin, longfile;
    FILE *pf;
    char *texto;
    char *palabra;


    if( !(pf= fopen(FILENAME, "rb")) ){
        fprintf(stderr, "No he podido abrir el archivo");
        return EXIT_FAILURE;
    }

    inicio = ftell(pf);
    fseek(pf, 0, SEEK_END);
    fin = ftell(pf);
    rewind (pf);
    longfile = fin - inicio;

    texto = (char *) malloc(longfile);
    fread(texto, sizeof(char), longfile, pf);

    printf ("Palabra para buscar: ");
    scanf (" %ms", &palabra);

    printf("%s", palabra);                     //Print word search

    printf("\n");

    for(int i=0; i < longfile; i++)            //Print text from file
        printf("%c", texto[i]);




    free(texto);
    free(palabra);
    fclose(pf);

    return EXIT_SUCCESS;
}

