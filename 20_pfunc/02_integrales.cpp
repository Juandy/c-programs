#include <stdio.h>
#include <stdlib.h>

#define INC 0.0001

double parabola (double x) { return x * x;}

double integral(
        double li, double ls,
        double(*pf) (double))
{
    double area = 0;
    for (double x = li; x<ls; x+=INC)
        area += INC * (*pf)(x);
}

// Función punto de entrada
int  main (int argc, char *argv[]){
    printf ("El area es: %lf\n", integral (1,3, &parabola));

    return EXIT_SUCCESS;
}
