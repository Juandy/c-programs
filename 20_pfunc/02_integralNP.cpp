#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define INC 0.0001

double polinomio (double coef, double x) {
    return pol (coef, x);
}

double pol (double *pol, int grado, double x){ //Evalua el valor de un polinomio, de coeficiente coef
    int lon = (grado + 1) * sizeof (double);   //en el punto x
    double *copia = (double *) malloc ( lon );
    memcpy (copia, pol,lon);

    for(int i= 0; i<grado; i++)
        for(int celda = 0; celda<=i; celda++)
            *(copia+i) *= x;
    for(int i=0; i<grado; i++)
        resultado += *(copia+i);

    free (copia);
}
double integral(
        double li, double ls,
        double(*pf) (double))
{
    double area = 0;
    for (double x = li; x<ls; x+=INC)
        area += INC * (*pf)(x);
}

double parabola (double x){
    double coef []=[1, 0, 0];
    return pol (coef, 2, x);
}

// Función punto de entrada
int  main (int argc, char *argv[]){

    printf ("f(2) = %lf", parabola(2));
    printf ("f(3) = %lf", parabola(3));
    printf ("El area es: %lf",integral (1,3 &parabola));

    return EXIT_SUCCESS;
}
