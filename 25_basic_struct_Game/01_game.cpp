#include <stdio.h>
#include <stdlib.h>

#define ENEM 10 //MAX Enemy

struct tVector{
    double x=1;
    double y=1;
};
struct tMovil{
    struct tVector pos;
    struct tVector vel;
    void (*mover)(struct tMovil * p);
};

void mover_lento (struct tMovil * p){
    p->pos.x += p->vel.x*0.5;
    p->pos.y += p->vel.y*0.5;
}
void mover_rapido (struct tMovil * p){
    p->pos.x += p->vel.x*2;
    p->pos.y += p->vel.y*2;
}


int  main (int argc, char *argv[]){

    struct tMovil *p;
    struct tMovil nave[ENEM];
    int enemigos = 0;

    while(enemigos < ENEM){
        p = (struct tMovil *)malloc(sizeof(struct tMovil));
        printf ("%lf \n",nave[enemigos].pos.x);
        enemigos++;
    }

    free(p);
    return EXIT_SUCCESS;
}
