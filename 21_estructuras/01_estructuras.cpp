#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 5
struct TiEmpleado{
  char nombre[MAX];
  double sueldo;
};

int  main (int argc, char *argv[]){

    struct TiEmpleado datos[N];

    for(int i=0;i<N;i++){
    printf ("Nombre: ");
    scanf  ("%s",datos[i].nombre);
    printf ("Sueldo: ");
    scanf  ("%lf",&datos[i].sueldo);
    }

    for(int i=0; i<N;i++)
    printf ("Hola %s tu sueldo es %.2lf€ \n",datos[i].nombre, datos[i].sueldo);

    return EXIT_SUCCESS;
}
