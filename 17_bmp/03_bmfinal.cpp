#include <stdio.h>
#include <stdlib.h>

#define Bpp   3

unsigned short width, height;
unsigned offset;
int row_size;

const char *five[] = { "█", "▓", "▒", "░", " " };

const char * five_colors (
        unsigned char r,
        unsigned char g,
        unsigned char b) {

    int media = (r + g + b ) / 3;
    return five[media / 51];
}

const char * two_colors (
        unsigned char r,
        unsigned char g,
        unsigned char b,
        int valim) {

    int media = (r + g + b ) / 3;
    return media > valim ? " " : "█";
}

void imprimir (unsigned char *image, int valim){
    system ("clear");
    for (int row=height; row>=0; row-- ){
        for (int col=0; col<width; col++) {
            unsigned char * px = &image[row_size * row + Bpp * col];
            printf ("%s", two_colors(
                        px[0],
                        px[1],
                        px[2], valim
                        ));
        }
        printf ("\n");
    }
}

int  main(int argc, char *argv[]){
    unsigned char *image;

    FILE *pf;
    if (!(pf = fopen (argv[1], "rb"))){
        fprintf (stderr, "Mac donde estas?\nRai no te veo.\n");
        return EXIT_FAILURE;
    }


    fseek (pf, 0xa, SEEK_SET);
    fread (&offset, 4, 1, pf);

    fseek (pf, 0x12, SEEK_SET);
    fread (&width, 2, 1, pf);

    row_size = (width * Bpp + 3 ) / 4 * 4;

    fseek (pf, 0x16, SEEK_SET);
    fread (&height, 2, 1, pf);

    image = (unsigned char *) malloc( height * row_size ) ;
    fseek (pf, offset, SEEK_SET);
    fread (image, 1, height * row_size, pf);
    fclose (pf);

    imprimir(image, 128);
    free (image);

    return EXIT_SUCCESS;
}

