#include <stdio.h>
#include <stdlib.h>

double factorial(double n, int p){
    if (p==1)
        return n;
    return n + 1. / factorial(n,p-1);
}

int  main (){

    double n;
    int p;

    printf ("valor de n: ");
    scanf  ("%lf",&n);
    printf ("valor de p: ");
    scanf  ("%i",&p);
    factorial(n,p);

    printf (" %.4lf \n",factorial(n,p));

    return EXIT_SUCCESS;
}
